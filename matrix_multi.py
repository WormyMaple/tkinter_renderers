'''
Some simple matrix operations using nested lists as matrices
Created by WormyMaple, 2023
'''


def multi(m1, m2):
    if (len(m1[0]) != len(m2)):
        print("Invalid Matrix input: " + str(m1, m2))
        return None

    new_matrix = []
    for i in range(len(m1)):
        new_matrix.append([None] * len(m2[0]))
    
    for i in range(len(m1)): # for row in m1
        for c in range(len(m2[0])): # for column in m2
            temp = 0
            for n in range(len(m1[0])): # for number in row and column
                temp += m1[i][n] * m2[n][c]
            
            new_matrix[i][c] = temp

    return new_matrix

def dist(m1, num):
    new_matrix = []

    for row in m1:
        new_row = []
        for column in row:
            new_row.append(column * num)
        
        new_matrix.append(new_row)
    
    return new_matrix