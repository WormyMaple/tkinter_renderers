'''
Simple isometric renderer made using the graphics.py tkinter wrapper
Created by WormyMaple, 2023
'''

import world_space as ws
import graphics as gl
import matrix_multi as mm
import math

class Iso_Object():
    def __init__(self, pos, scale, rot, points, edges, faces, colors, edge_color = "black"):
        self.pos = pos
        self.scale = scale
        self.rotation = rot

        self.points = points.copy()
        self.edges = edges.copy()
        self.faces = faces.copy()

        self.colors = colors
        self.edge_color = edge_color
    
    def render(self, win):
        # - rotate vertices via a rotation matrix -
        x, y, z = self.rotation.x, self.rotation.y, self.rotation.z

        # rotation matrix for this object
        rotation_m = [[math.cos(z) * math.cos(y), (math.cos(z) * math.sin(y) * math.sin(x)) - (math.sin(z) * math.cos(x)), (math.cos(z) * math.sin(y) * math.cos(x)) + (math.sin(z) * math.sin(x))],
                      [math.sin(z) * math.cos(y), (math.sin(z) * math.sin(y) * math.sin(x)) + (math.cos(z) * math.cos(x)), (math.sin(z) * math.sin(y) * math.cos(x)) - (math.cos(z) * math.sin(x))],
                      [-math.sin(y), math.cos(y) * math.sin(x), math.cos(y) * math.cos(x)]]

        rot_points = []

        for point in self.points:
            scaled_point = point.copy()

            scaled_point.x *= self.scale.x
            scaled_point.y *= self.scale.y
            scaled_point.z *= self.scale.z

            point_m = [[scaled_point.x], 
                       [scaled_point.y],
                       [scaled_point.z]]

            # calculate rotated vertex by multiplying [rot] and [rotation_m]
            rotated_m = mm.multi(rotation_m, point_m)

            resultant_point = ws.Vector3(rotated_m[0][0], rotated_m[1][0], rotated_m[2][0])

            # add world pos
            resultant_point.x += world_pos.x
            resultant_point.y += world_pos.y
            resultant_point.z += world_pos.z

            rot_points.append(resultant_point)

        projected_points = []
        world_points = []

        object_collection = []

        # point (vertex) rendering
        for point in rot_points:
            point = ws.Vector3(point.x + self.pos.x, point.y + self.pos.y, point.z + self.pos.z) # offset to position
            world_points.append(point)

            projected_point = project(point)
            projected_point.draw(win)

            projected_points.append(projected_point)
            object_collection.append(projected_point)
        
        # edge rendering
        for edge in self.edges:
            p1 = projected_points[edge[0]]
            p2 = projected_points[edge[1]]

            new_line = gl.Line(p1, p2)
            new_line.setFill(self.edge_color)
            new_line.draw(win)

            object_collection.append(new_line)

        # face rendering
        face_dists = []

        # - Get mid_points
        for face in self.faces:
            avg_x = 0
            avg_y = 0
            avg_z = 0

            for point in face:
                world_point = world_points[point]

                avg_x += world_point.x
                avg_y += world_point.y
                avg_z += world_point.z
            
            point_count = len(face)

            avg_point = ws.Vector3(avg_x / point_count, avg_y / point_count, avg_z / point_count)

            face_dists.append(avg_point.dist_to(camera_pos))

        # sort faces
        sorted_faces = [x for _,x in sorted(zip(face_dists, self.faces))]
        sorted_faces.reverse()

        for face in sorted_faces:
            face_points = []

            for point in face:
                face_points.append(projected_points[point])
            
            new_face = gl.Polygon(face_points)
            new_face.setFill(self.colors[self.faces.index(face) % len(colors)])
            new_face.draw(win)

            object_collection.append(new_face)

        return object_collection


# convert point from ([0, x], [0, y]) base coordinates to proper ([-x1, x2], [-y1, y2]) cartesian coordinates
def to_origin(bounds, point):
    return gl.Point((point.getX() * camera_scale.x) + (bounds[0] / 2), bounds[1] / 2 - (point.getY() * camera_scale.y))

# project world point to isometric view
def project(point):
    isoed_point = mm.multi(mm.dist(iso_m, root_6), point.matrix()) # Make point isometric
    point_2d = mm.multi(m_2d, isoed_point) # Cast 3d point to 2d position
    projected_point = to_origin(bounds, gl.Point(point_2d[0][0], point_2d[1][0]))

    return projected_point

bounds = (1280, 720)

# screen initialization
gl_win = gl.GraphWin("", bounds[0], bounds[1], autoflush=False)
gl_win.setBackground("black")

# values to use for projection later
root_6 = 1 / math.sqrt(6)
root_3 = math.sqrt(3)
root_2 = math.sqrt(2)

# isometric projection matrix
iso_m = [[root_3, 0, -root_3],
         [1, 2, 1],
         [root_2, -root_2, root_2]]

# 2d projection matrix
m_2d = [[1, 0, 0],
        [0, 1, 0],
        [0, 0, 0]]

# values for cube
cube_points = [ws.Vector3(-50, 50, -50),
                 ws.Vector3(50, 50, -50),
                 ws.Vector3(50, -50, -50),
                 ws.Vector3(-50, -50, -50),
                 ws.Vector3(-50, 50, 50),
                 ws.Vector3(50, 50, 50),
                 ws.Vector3(50, -50, 50),
                 ws.Vector3(-50, -50, 50)]
cube_edges = [(0, 1),
                (0, 3),
                (0, 4),
                (1, 2),
                (1, 5),
                (2, 3),
                (2, 6),
                (3, 7),
                (4, 5),
                (4, 7),
                (5, 6),
                (6, 7)]

cube_faces = [(0, 1, 2, 3),
                (0, 1, 5, 4),
                (0, 3, 7, 4),
                (1, 2, 6, 5),
                (2, 3, 7, 6),
                (4, 5, 6, 7)]

# - create objects -
objects = []

colors = ["red", gl.color_rgb(0, 255, 0), "blue", "white", "yellow", "orange"]

box = Iso_Object(ws.Vector3(0, 0, 0), ws.Vector3(1, 1, 1), ws.Vector3(0, 0, 0), cube_points, cube_edges, cube_faces, colors)
box_1 = Iso_Object(ws.Vector3(200, 0, 0), ws.Vector3(1, 1, 1), ws.Vector3(0, 0, 0), cube_points, cube_edges, cube_faces, colors)
box_2 = Iso_Object(ws.Vector3(0, 0, 200), ws.Vector3(1, 1, 1), ws.Vector3(0, math.pi / 4, 0), cube_points, cube_edges, cube_faces, colors)

objects.append(box)
objects.append(box_1)
objects.append(box_2)

drawn_objects = []

# timekeeping
t = 0
t_step = 0.01

# world values
world_pos = ws.Vector3(0, 0, 0)
camera_pos = ws.Vector3(-10000, 10000, -10000)
camera_scale = ws.Vector2(1, 1)

# main loop
while True:
    # undraw object collection
    for object_group in drawn_objects:
        for object in object_group:
            object.undraw()
    drawn_objects = []

    # rotation go brb
    for i in range(1, len(objects) + 1):
        rot_step = i * t_step

        objects[i - 1].rotation.x += rot_step
        objects[i - 1].rotation.y -= rot_step * 2
        objects[i - 1].rotation.y += rot_step * 3

    # draw objects
    for object in objects:
        drawn_objects.append(object.render(gl_win))

    gl.update(60)

    t += t_step