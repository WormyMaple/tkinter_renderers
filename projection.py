'''
Very basic perspective projection using the graphics.py tkinter wrapper
Created by WormyMaple, 2023
'''

import graphics as gl
import math
import random
from world_space import *
import matrix_multi
from perlin_noise import PerlinNoise

# simple, single axis rotator
def rotate_y(point, theta, center = Vector3(0, 0, 0)):
    rot_matrix = [[math.cos(theta), math.sin(theta)],
                  [-math.sin(theta), math.cos(theta)]]
    
    target_matrix = [[point.x - center.x], [point.z - center.z]]
    rotated_matrix = matrix_multi.multi(rot_matrix, target_matrix)

    return Vector3(rotated_matrix[0][0] + center.x, point.y, rotated_matrix[1][0] + center.z)

class Object():
    def __init__(self, position, points, edges, faces, y_rot):
        self.position = position
        self.y_rot = y_rot

        self.base_points = points.copy()
        self.points = points.copy()
        self.edges = edges
        self.faces = faces
    
    def render(self):
        object_collection = []

        screen_points = []

        # - draw vertices -
        world_vertices = []
        vertex_dists = []

        # rotate points
        for point in self.points:
            world_point = rotate_y(Vector3(point.x + self.position.x, point.y + self.position.y, point.z + self.position.z), self.y_rot, self.position)
            world_vertices.append(world_point)
        # convert vertices to screen point
        for vertex in world_vertices:
            screen_point = project(vertex)
            screen_points.append(screen_point)

            vertex_dists.append(vertex.dist())
        
        # draw edges between vertices
        if len(self.edges) > 0:
            for edge in self.edges:
                new_edge = gl.Line(screen_points[edge[0]], screen_points[edge[1]])
                new_edge.setFill("white")
                new_edge.draw(gl_win)

                object_collection.append(new_edge)

        # - draw faces -
        face_dists = []
        midpoints = []

        # get midpoint of all faces
        for face in self.faces:
            vert_count = len(face)

            mid_point_x = 0
            mid_point_y = 0
            mid_point_z = 0

            for point in face:
                mid_point_x += world_vertices[point].x
                mid_point_y += world_vertices[point].y
                mid_point_z += world_vertices[point].z

            mid_point = Vector3(mid_point_x / vert_count, mid_point_y / vert_count, mid_point_z / vert_count)

            face_dists.append(mid_point.dist())
            midpoints.append(mid_point)

        if len(self.faces) > 0:
            # sort faces far -> near
            _, to_draw_faces = zip(*sorted(zip(face_dists, self.faces)))
            to_draw_faces = list(reversed(to_draw_faces))

            # draw face on screen
            for i in range(len(to_draw_faces)):
                face_points = []
                for point in to_draw_faces[i]:
                    face_points.append(screen_points[point])

                new_face = gl.Polygon(face_points)

                color = colors[(self.faces.index(to_draw_faces[i])) % len(colors)]

                new_face.setFill(color)
                new_face.setOutline(color)
                new_face.draw(gl_win)
                object_collection.append(new_face)

        # return drawn objects for destruction on next frame
        return object_collection

    # function to deform vertices of an object
    def vertex_offset(self, offsets):
        for i in range(len(self.base_points)):
            new_point = self.base_points[i]
            offset = offsets[i]

            self.points[i] = Vector3(new_point.x + offset.x, new_point.y + offset.y, new_point.z + offset.z)

# convert point from ([0, x], [0, y]) base coordinates to proper ([-x1, x2], [-y1, y2]) cartesian coordinates
def cartesian(point):
    return gl.Point(point.getX() + origin[0], point.getY() + origin[1])

# project world point to screen point
def project(point):
    target_ncp = bounds[0] / ncp_width

    x = ((point.x * near_clip_plane) / -point.z) * target_ncp
    y = ((point.y * near_clip_plane) / -point.z) * target_ncp

    projected_point = cartesian(gl.Point(x, y))

    return projected_point

bounds = (1920, 1080)
origin = (bounds[0] / 2, bounds[1] / 2)

blues = [gl.color_rgb(0, 50, 255), "blue2", "blue3"]

# screen initialization
gl_win = gl.GraphWin("projection", bounds[0], bounds[1], autoflush=False)
gl_win.setBackground("black")

# near clip values
near_clip_plane = 10
ncp_width = 25 # near clip scale (nearly FOV)

drawn_objects = []

# generate grid points
grid_points = []
grid_edges = []
grid_faces = []

grid_width = 15
grid_dist = 20
grid_depth = -50

grid_origin = (grid_width * grid_dist) / -2

colors = []

# grid colors
for i in range((grid_width ** 2) * 2 + 6):
    colors.append(random.choice(blues))

# grid points
for i in range(grid_width):
    for j in range(grid_width):
        grid_points.append(Vector3(grid_origin + (i * grid_dist), 0, grid_origin + j * grid_dist))

# top faces
for i in range(grid_width - 1):
    for j in range(grid_width - 1):
        x = i * grid_width
        x_1 = (i + 1) * grid_width

        face_1 = [x + j, x + j + 1, x_1 + j]
        face_2 = [x_1 + j + 1, x + j + 1, x_1 + j]

        grid_faces.append(face_1)
        grid_faces.append(face_2)

# list of objects to render
objects = [Object(Vector3(0, -75, 300), grid_points.copy(), [], grid_faces, math.pi / 4)]

# time keeping
tick = 0
tick_step = 0.01

# instantiate perlin noise
noise = PerlinNoise(octaves=10, seed=1)

# main loop
while True:
    for object_group in drawn_objects:
        for drawn_object in object_group:
            drawn_object.undraw()

    drawn_objects = []

    # grid deformation
    grid_offsets = []

    for i in range(grid_width + 4):
        for j in range(grid_width):
            noise_val = noise([i / 75 + tick / 10, j / 75 + tick / 10])
            grid_offsets.append(Vector3(0, noise_val * 60, 0))
    
    objects[0].vertex_offset(grid_offsets)

    objects[0].y_rot = tick / 2

    for i in range(len(objects)):
        scene_object = objects[i]

        drawn_objects.append(scene_object.render())

    gl.update(60)
    tick += tick_step
