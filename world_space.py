'''
Standard coordinate objects with helpful functions
Created by WormyMaple, 2023
'''

import math

class Vector3():
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def to_list(self):
        return [self.x, self.y, self.z]
    
    def dist(self):
        return math.sqrt((self.x ** 2) + (self.y ** 2) + (self.z ** 2))
        
    def dist_to(self, vect):
        return math.sqrt(((self.x - vect.x) ** 2) + ((self.y - vect.y) ** 2) + ((self.z - vect.z) ** 2))

    def matrix(self):
        return [[self.x], [self.y], [self.z]]
    
    def print_v(self):
        return f"{self.x}, {self.y}, {self.z}"
    
    def copy(self):
        return Vector3(self.x, self.y, self.z)

class Vector2():
    def __init__(self, x, y):
        self.x = x
        self.y = y