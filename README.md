# tkinter_renderers
A couple of simple renderers that use the John Zelle's tkinter wrapper.

## Isometric Renderer
![isometric_renderer](images/iso_screenshot.png)
This renderer can render simple meshes from a isometric perspective
- Support for full-axis rotation
- Camera uses a dynamic position

## Perspective Renderer
![perspective_renderer](images/persp_screenshot.png)
This renderer can render simple meshes with a perspective projection
- Supports y-axis rotation
- Supports mesh deformation
- Requires "perlin_noise" package to generate offsets for wave demonstration

Created by WormyMaple, 2023